<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-typo_guillemets
// Langue: fr
// Date: 25-09-2019 18:29:45
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'typo_guillemets_description' => 'Ce plugin remplace automatiquement les guillemets droits (") par les guillemets typographiques («»“”„) de la langue de composition et guillemette correctement la balise <code><q></code>. Les liens automatiques <code>[->1]</code> vers des articles dont le titre contient des «guillemets français» passent en guillemets “de second niveau”. Le remplacement, transparent pour l\'utilisateur, ne modifie pas le texte mais seulement l\'affichage final.',
	'typo_guillemets_slogan' => 'Correction automatique des guillemets, selon la langue',
);
?>